import json
from blockchain import Blockchain
from flask import Flask, jsonify, request


app = Flask(__name__)

blockChainObj = Blockchain()


#Mine a new block
#requestmethod	POST
#responsebody ContentType	JSON
@app.route('/blockchain/mine', methods=['POST'])
def mineBlock():
	data = request.form['data']
	blockChainObj.createBlock(data)
	response = {'newBlock': blockChainObj.getBlockChain()[-1],
					'length': len(blockChainObj.getBlockChain())}
	return jsonify(response), 200


#Get blockchain
#requestmethod	GET
#responsebody ContentType	JSON
@app.route('/blockchain/get', methods=['GET'])
def getBlockChain():
	response = {'blockchain': blockChainObj.getBlockChain(),
					'length': len(blockChainObj.getBlockChain())}
	return jsonify(response), 200


#Set blockchain
#requestmethod	POST
#requestbody	
#responsebody ContentType	JSON
@app.route('/blockchain/set', methods=['POST'])
def setBlockChain():
	blockChain = request.form['blockChain']
	blockChainObj.setBlockChain(json.loads(blockChain))
	response = {"message": "Blockchain accepted"}
	return jsonify(response), 200


#Validate blockchain
#requestmethod	GET/ POST
#requestbody	
#response	ContentType		JSON
@app.route('/blockchain/validate/', methods=['GET', 'POST'])
def validateBlockChain():
	savedBlockChain = blockChainObj.getBlockChain()
	if request.method == 'POST':
		blockChain = json.loads(request.form['blockChain'])
		blockChainObj.setBlockChain(blockChain)

	if blockChainObj.isChainValid(blockChainObj.getBlockChain()):
		response = {'status': True}
	else:
		response = {'status': False}

	blockChainObj.setBlockChain(savedBlockChain)
	return jsonify(response), 200



app.run(host='0.0.0.0', port='8080')
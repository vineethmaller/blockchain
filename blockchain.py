import datetime
import hashlib
import json


class Blockchain:

	#No argument constructor
	def __init__(self):
		self.chain = []
		self.createBlock(data = '')	#Creates a genesis block


	#Set a blockchain
	#input	list	blockChain
	def setBlockChain(self, blockChain):
		self.chain = blockChain


	#Get a blockchain
	#output	list
	def getBlockChain(self):
		return self.chain


	#Creates a new block
	#input	string		data			Data to be contained in the new block
	#output void	
	def createBlock(self, data):
		block = {'index': len(self.chain)+1,
					'timestamp': str(datetime.datetime.now()),
					'data': data}
		if len(self.chain) == 0:
			block['previousHash'] = '0'
		else:
			block['previousHash'] = self.chain[block['index']-2]['hash']

		block = self.mineBlock(block)
		self.chain.append(block);


	#Mines a block
	#input 	dictionary	partialblock 	A block without nonce and hash 
	#output dictionary
	def mineBlock(self, partialBlock):
		nonce = 1
		checkProofOfWork = False

		while checkProofOfWork is False:
			partialBlock['nonce'] = str(nonce)
			newHash = hashlib.sha256(json.dumps(partialBlock, sort_keys = True).encode()).hexdigest()
			if newHash[:1] == partialBlock['previousHash'][:1]:
				checkProofOfWork = True
				partialBlock['hash'] = newHash
			else:
				nonce += 1

		return partialBlock


	#Check if blockchain is valid
	#input	list		blockChain 		A list of blocks
	#output boolean
	def isChainValid(self, blockChain):
		index = 1

		while index<=len(blockChain):
			block = blockChain[index-1]

			if block['index'] == index:
				if block['index'] != 1:
					prevBlock = blockChain[index-2];
					if block['previousHash'] != prevBlock['hash']:
						return False
				else:
					if block['previousHash'] != '0':
						return False
				newBlock = {'index': block['index'],
								'timestamp': block['timestamp'],
								'data': block['data'],
								'previousHash': block['previousHash'],
								'nonce': block['nonce']}
				calculatedHash = hashlib.sha256(json.dumps(newBlock, sort_keys = True).encode()).hexdigest()
				if (block['hash'] == calculatedHash) and (calculatedHash[:1] == block['previousHash'][:1]):
					index += 1
				else:
					return False
			else:
				return False
		return True
